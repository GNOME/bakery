# Low German translation for bakery.
# Copyright (C) 2010 bakery's COPYRIGHT HOLDER
# This file is distributed under the same license as the bakery package.
# Nils-Christoph Fiedler <fiedler@medienkompanie.de>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: bakery master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=bakery&component=general\n"
"POT-Creation-Date: 2010-02-18 02:59+0000\n"
"PO-Revision-Date: 2010-03-04 00:49+0100\n"
"Last-Translator: Nils-Christoph Fiedler <fiedler@medienkompanie.de>\n"
"Language-Team: Low German <nds-lowgerman@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../bakery/App/App_WithDoc.cc:97
#: ../bakery/App/App_WithDoc.cc:189
msgid "Open failed."
msgstr "Opmaken ging in dutt."

#: ../bakery/App/App_WithDoc.cc:97
#: ../bakery/App/App_WithDoc.cc:189
msgid "The document could not be opened."
msgstr "Dat Dokument künn nich opmakt werrn."

#. Tell user that it's already open:
#: ../bakery/App/App_WithDoc.cc:136
msgid "Document already open"
msgstr "Dokument all opmakt"

#: ../bakery/App/App_WithDoc.cc:136
msgid "This document is already open."
msgstr "Dat Dokument is all opmakt."

#. The save failed. Tell the user and don't do anything else:
#: ../bakery/App/App_WithDoc.cc:282
#: ../bakery/App/App_WithDoc.cc:333
msgid "Save failed."
msgstr "Spiekern gin in dutt."

#: ../bakery/App/App_WithDoc.cc:282
#: ../bakery/App/App_WithDoc.cc:333
msgid "There was an error while saving the file. Your changes have not been saved."
msgstr ""

#: ../bakery/App/App_Gtk.cc:247
msgid "File"
msgstr "Datei"

#: ../bakery/App/App_Gtk.cc:290
msgid "_Edit"
msgstr "_Bewarken"

#: ../bakery/App/App_Gtk.cc:333
msgid "_Help"
msgstr "_Hölp"

#: ../bakery/App/App_Gtk.cc:337
msgid "_About"
msgstr "_Över"

#: ../bakery/App/App_Gtk.cc:337
msgid "About the application"
msgstr "Över düsses Programm"

#: ../bakery/App/App_WithDoc_Gtk.cc:150
msgid "_File"
msgstr "_Datei"

#: ../bakery/App/App_WithDoc_Gtk.cc:151
msgid "_Recent Files"
msgstr "_Leste Dateien"

#: ../bakery/App/App_WithDoc_Gtk.cc:227
msgid " (read-only)"
msgstr "[Just Lesen]"

#: ../bakery/App/Dialog_OfferSave.cc:30
msgid "This document has unsaved changes. Would you like to save the document?"
msgstr ""

#: ../bakery/App/Dialog_OfferSave.cc:32
msgid ""
"\n"
"\n"
"Document:\n"
msgstr ""
"\n"
"\n"
"Dokument:\n"

#: ../bakery/App/Dialog_OfferSave.cc:45
msgid "Close without Saving"
msgstr "Sluten ohn to spiekern"

#: ../bakery/App/Dialog_OfferSave.cc:54
msgid "Discard"
msgstr "Avbreken"

#: ../bakery/App/GtkDialogs.cc:65
msgid "Open Document"
msgstr "Dokument opmaken"

#: ../bakery/App/GtkDialogs.cc:125
msgid "Save Document"
msgstr "Dokument spiekern"

#. Warn the user:
#: ../bakery/App/GtkDialogs.cc:188
msgid "Read-only File."
msgstr "Just-Lesen Datei."

#: ../bakery/App/GtkDialogs.cc:188
msgid "You may not overwrite the existing file, because you do not have sufficient access rights."
msgstr ""

#. Warn the user:
#: ../bakery/App/GtkDialogs.cc:202
msgid "Read-only Directory."
msgstr "Just-Lesen Verteeknis."

#: ../bakery/App/GtkDialogs.cc:202
msgid "You may not create a file in this directory, because you do not have sufficient access rights."
msgstr ""

#: ../bakery/Configuration/Dialog_Preferences.cc:29
msgid "Preferences"
msgstr "Instellens"

#: ../bakery/Document/Document.cc:360
msgid "Untitled"
msgstr "Ohn Titel"

#: ../examples/Configuration/ConfClient/examplewindow.cc:26
msgid "Instant-apply"
msgstr "Foorts-tostimmen"

#: ../examples/Configuration/ConfClient/examplewindow.cc:26
msgid "Load/Save"
msgstr "Laden/Spiekern"

#: ../examples/Configuration/ConfClient/examplewindow.cc:28
msgid "foo"
msgstr "foo"

#: ../examples/Configuration/ConfClient/examplewindow.cc:28
msgid "bar"
msgstr "Balken"

#: ../examples/Configuration/Dialog_Preferences/examplewindow.cc:27
msgid "Preferences (apply on close)"
msgstr "Instellen (tostimmen bi'm Sluten)"

#: ../examples/Configuration/Dialog_Preferences/examplewindow.cc:28
msgid "Preferences (instant apply)"
msgstr "Instellen (just tostimmen)"

#: ../examples/WithDoc/appexample.cc:47
#: ../examples/WithDocView/appexample.cc:39
#: ../examples/WithDocViewComposite/appexample.cc:39
msgid "A Bakery example."
msgstr "Een Bakery Bispeel."

#: ../examples/WithXmlDoc/appexample.cc:39
msgid "A Bakery XML example."
msgstr "Een Bakery XML Bispeel."

#: ../examples/WithXmlDocGlade/appexample.cc:49
msgid "A Bakery XML example, also using libglademm."
msgstr "Een Bakery XML Bispeel, brukt ook libglademm."

#: ../examples/WithoutDoc/appexample.cc:43
msgid "(C) 2000 Murray Cumming"
msgstr "(C) 2000 Murray Cumming"

#: ../examples/WithoutDoc/appexample.cc:43
msgid "A Bakery example"
msgstr "Een Bakery Bispeel"

