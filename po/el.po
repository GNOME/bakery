# translation of el.po to Greek
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) 2005 THE PACKAGE'S COPYRIGHT HOLDER.
# Kostas Papadimas <pkst@gnome.org>, 2005.
#
# Μάριος Ζηντίλης <m.zindilis@dmajor.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: el\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=bakery&component=general\n"
"POT-Creation-Date: 2009-04-16 17:40+0000\n"
"PO-Revision-Date: 2010-02-01 21:58+0200\n"
"Last-Translator: Μάριος Ζηντίλης <m.zindilis@dmajor.org>\n"
"Language-Team: Greek <team@gnome.gr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#: ../bakery/App/App_WithDoc.cc:97 ../bakery/App/App_WithDoc.cc:189
msgid "Open failed."
msgstr "Αποτυχία ανοίγματος"

#: ../bakery/App/App_WithDoc.cc:97 ../bakery/App/App_WithDoc.cc:189
msgid "The document could not be opened."
msgstr "Δεν ήταν δυνατό το άνοιγμα του αρχείου"

#. Tell user that it's already open:
#: ../bakery/App/App_WithDoc.cc:136
msgid "Document already open"
msgstr "Το έγγραφο είναι ήδη ανοικτό"

#: ../bakery/App/App_WithDoc.cc:136
msgid "This document is already open."
msgstr "Το έγγραφο είναι ήδη ανοικτό"

#. The save failed. Tell the user and don't do anything else:
#: ../bakery/App/App_WithDoc.cc:282 ../bakery/App/App_WithDoc.cc:333
msgid "Save failed."
msgstr "Η αποθήκευση απέτυχε."

#: ../bakery/App/App_WithDoc.cc:282 ../bakery/App/App_WithDoc.cc:333
msgid ""
"There was an error while saving the file. Your changes have not been saved."
msgstr ""
"Σφάλμα κατά την αποθήκευση του αρχείου. Οι αλλαγές σας δεν αποθηκεύθηκαν."

#: ../bakery/App/App_Gtk.cc:247
#| msgid "_File"
msgid "File"
msgstr "Αρχείο"

#: ../bakery/App/App_Gtk.cc:290
msgid "_Edit"
msgstr "_Επεξεργασία"

#: ../bakery/App/App_Gtk.cc:333
msgid "_Help"
msgstr "_Βοήθεια"

#: ../bakery/App/App_Gtk.cc:337
msgid "_About"
msgstr "Π_ερί"

#: ../bakery/App/App_Gtk.cc:337
msgid "About the application"
msgstr "Περί αυτής της εφαρμογής"

#: ../bakery/App/App_WithDoc_Gtk.cc:150
msgid "_File"
msgstr "_Αρχείο"

#: ../bakery/App/App_WithDoc_Gtk.cc:151
msgid "_Recent Files"
msgstr "Πρόσ_φατα αρχεία"

#: ../bakery/App/App_WithDoc_Gtk.cc:227
msgid " (read-only)"
msgstr "(μόνο για ανάγνωση)"

#: ../bakery/App/Dialog_OfferSave.cc:30
msgid "This document has unsaved changes. Would you like to save the document?"
msgstr ""
"Το έγγραφο περιέχει μη αποθηκευμένες αλλαγές. Θέλετε να αποθηκεύσετε το "
"έγγραφο;"

#: ../bakery/App/Dialog_OfferSave.cc:32
msgid ""
"\n"
"\n"
"Document:\n"
msgstr ""
"\n"
"\n"
"Έγγραφο:\n"

#: ../bakery/App/Dialog_OfferSave.cc:45
msgid "Close without Saving"
msgstr "Κλείσιμο χωρίς αποθήκευση"

#: ../bakery/App/Dialog_OfferSave.cc:54
msgid "Discard"
msgstr "Α_πόρριψη"

#: ../bakery/App/GtkDialogs.cc:65
#| msgid ""
#| "\n"
#| "\n"
#| "Document:\n"
msgid "Open Document"
msgstr "Άνοιγμα εγγράφου"

#: ../bakery/App/GtkDialogs.cc:125
#| msgid ""
#| "\n"
#| "\n"
#| "Document:\n"
msgid "Save Document"
msgstr "Αποθήκευση εγγράφου"

#. Warn the user:
#: ../bakery/App/GtkDialogs.cc:188
msgid "Read-only File."
msgstr "Αρχείο μόνο για ανάγνωση."

#: ../bakery/App/GtkDialogs.cc:188
msgid ""
"You may not overwrite the existing file, because you do not have sufficient "
"access rights."
msgstr ""
"Δε μπορείτε να αντικαταστήσετε το υπάρχον αρχείο γιατί δεν έχετε τα "
"απαραίτητα δικαιώματα πρόσβασης."

#. Warn the user:
#: ../bakery/App/GtkDialogs.cc:202
msgid "Read-only Directory."
msgstr "Φάκελος μόνο για ανάγνωση."

#: ../bakery/App/GtkDialogs.cc:202
msgid ""
"You may not create a file in this directory, because you do not have "
"sufficient access rights."
msgstr ""
"Δεν μπορείτε να δημιουργήσετε ένα αρχείο σε αυτόν το φάκελο γιατί δεν έχετε "
"απαραίτητα δικαιώματα πρόσβασης."

#: ../bakery/Configuration/Dialog_Preferences.cc:29
msgid "Preferences"
msgstr "Προτιμήσεις"

#: ../bakery/Document/Document.cc:360
msgid "Untitled"
msgstr "Ανώνυμο"

#: ../examples/Configuration/ConfClient/examplewindow.cc:26
msgid "Instant-apply"
msgstr "Άμεση εφαρμογή"

#: ../examples/Configuration/ConfClient/examplewindow.cc:26
msgid "Load/Save"
msgstr "Φόρτωση/Αποθήκευση"

#: ../examples/Configuration/ConfClient/examplewindow.cc:28
msgid "foo"
msgstr "foo"

#: ../examples/Configuration/ConfClient/examplewindow.cc:28
msgid "bar"
msgstr "bar"

#: ../examples/Configuration/Dialog_Preferences/examplewindow.cc:27
msgid "Preferences (apply on close)"
msgstr "Προτιμήσεις (να εφαρμοστούν στο κλείσιμο)"

#: ../examples/Configuration/Dialog_Preferences/examplewindow.cc:28
msgid "Preferences (instant apply)"
msgstr "Προτιμήσεις (να εφαρμοστούν άμεσα)"

#: ../examples/WithDoc/appexample.cc:47
#: ../examples/WithDocView/appexample.cc:39
#: ../examples/WithDocViewComposite/appexample.cc:39
msgid "A Bakery example."
msgstr "Ένα παράδειγμα του Bakery."

#: ../examples/WithXmlDoc/appexample.cc:39
msgid "A Bakery XML example."
msgstr "Ένα παράδειγμα XML του Bakery."

#: ../examples/WithXmlDocGlade/appexample.cc:49
msgid "A Bakery XML example, also using libglademm."
msgstr ""
"Ένα παράδειγμα XML του Bakery το οποίο χρησιμοποιεί επίσης τη libglademm."

#: ../examples/WithoutDoc/appexample.cc:43
msgid "(C) 2000 Murray Cumming"
msgstr "(C) 2000 Murray Cumming"

#: ../examples/WithoutDoc/appexample.cc:43
msgid "A Bakery example"
msgstr "Ένα παράδειγμα του Bakery"
