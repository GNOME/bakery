// -*- C++ -*-

/* docexample.cc
 * 
 * Copyright (C) 2000 Murray Cumming  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "docexample.h"

DocExample::DocExample()
{
  set_file_extension("bakery_example_xml");

  set_dtd_name("example_xml_doc.dtd");
  set_dtd_root_node_name("example_xml_doc");
}

DocExample::~DocExample()
{
}

void DocExample::set_something(const std::string& strSomething)
{
  try
  {
    set_node_attribute_value(get_node_something(), "someval", strSomething);
    set_modified();
  }
  catch(const std::exception& ex)
  {
    handle_exception(ex); 
  }
}


std::string DocExample::get_something()
{
  std::string strValue;

  try
  {
    strValue = get_node_attribute_value(get_node_something(), "someval");
  }
  catch(const std::exception& ex)
  {
    handle_exception(ex); 
  }

  return strValue;
}

xmlpp::Element* DocExample::get_node_something()
{
  try
  { 
    xmlpp::Element* nodeRoot = get_node_document();
    return get_node_child_named_with_add(nodeRoot, "something");
  }
  catch(const std::exception& ex)
  {
    handle_exception(ex);
    return 0;
  }
}

void DocExample::handle_exception(const std::exception& ex) const
{
  std::cerr << "Exception: " << ex.what() << std::endl;
}

