// -*- C++ -*-

/* docexample.h
 * 
 * Copyright (C) 2000 Murray Cumming  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef HEADER_DOC_EXAMPLE
#define HEADER_DOC_EXAMPLE

#include "bakery/Document/Document_XML.h"

class DocExample : public Bakery::Document_XML
{
public:
  DocExample();
  virtual ~DocExample();

  void set_something(const std::string& strSomething);
  std::string get_something();

protected:
   virtual xmlpp::Element* get_node_something(); //Gets <something>

   virtual void handle_exception(const std::exception& ex) const;

};

#endif //HEADER_DOC_EXAMPLE
