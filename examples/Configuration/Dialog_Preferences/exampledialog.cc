//$Id$ -*- c++ -*-

/* gtkmm example Copyright (C) 2002 gtkmm development team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "exampledialog.h"
#include <iostream>

ExampleDialog::ExampleDialog(Gtk::Window& parent, bool instant)
: Bakery::Dialog_Preferences(parent, "/apps/gnome/bakery_examples/dialog_preferences", "example.glade", "vbox", instant)
{
  //Associate configuration keys with glade widget names.
  connect_widget("some_text_preference", "entry");
  connect_widget("some_bool_preference", "checkbox");
  connect_widget("some_range_preference", "hscale");
  connect_widget("some_int_preference", "optionmenu");
  connect_widget("some_string_preference", "combo");
  connect_widget("some_float_preference", "spinbutton");

  show_all_children();
}

ExampleDialog::~ExampleDialog()
{

}
