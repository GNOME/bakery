// -*- C++ -*-

/* subview2.h
 * 
 * Copyright (C) 2000 Murray Cumming  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef HEADER_SUBVIEW2
#define HEADER_SUBVIEW2

#include <gtkmm/table.h>
#include "docexample.h"
#include <gtkmm/comboboxtext.h>

class SubView2 : 
  public Gtk::Table,
  public Bakery::View<DocExample>
{
public:
  SubView2();
  virtual ~SubView2();

  //overrrides:
  virtual void load_from_document();
  virtual void save_to_document();

protected:

  //Signal handlers:
  virtual void on_Combo_changed();

  //Child widgets:
  Gtk::Label m_Label;
  Gtk::ComboBoxText m_Combo;
};


#endif //HEADER_SUBVIEW2
