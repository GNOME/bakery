// -*- C++ -*-

/* docexample.cc
 * 
 * Copyright (C) 2000 Murray Cumming  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "docexample.h"

DocExample::DocExample()
{
  set_file_extension("bakery_withdocviewcomposite");
}

DocExample::~DocExample()
{
}

bool DocExample::load_after()
{
  bool bTest = Bakery::Document::load();

  if(bTest)
  {
	  //See comment in save_before().
	  Glib::ustring strData = get_contents();
	  Glib::ustring::size_type pos = strData.find("****");

	  if(pos != Glib::ustring::npos)
	  {
	    m_strSomething = strData.substr(0, pos);
	    m_strSomethingElse = strData.substr(pos+4);
	  }
	  else
	  {
	    return false; //failed: invalid format.
	  }
  } 

  return bTest; 
}

bool DocExample::save_before()
{
  //This is a very silly file format.
  //I suggest that you use the Document_XML class.
  Glib::ustring strData = get_something();
  strData += "****";
  strData += get_something_else();
  
  set_contents(strData);
  return Bakery::Document::save_before();
}

void DocExample::set_something(const Glib::ustring& strSomething)
{
  if(m_strSomething != strSomething)
  {
    m_strSomething = strSomething;
    set_modified();
  }
}

void DocExample::set_something_else(const Glib::ustring& strSomethingElse)
{
  if(m_strSomethingElse != strSomethingElse)
  {
    m_strSomethingElse = strSomethingElse;
    set_modified();
  }
}

Glib::ustring DocExample::get_something() const
{
  return m_strSomething;
}

Glib::ustring DocExample::get_something_else() const
{
  return m_strSomethingElse;
}
