// -*- C++ -*-

/* appexample.cc
 * 
 * Copyright (C) 2000 Murray Cumming 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"
#include "appexample.h"
#include <glibmm/i18n.h>

AppExample::AppExample()
  : Bakery::App_Gtk("WithoutDoc"),
  m_Label("blah")
{
  m_Box.pack_start(m_Label, Gtk::PACK_EXPAND_WIDGET);
  m_Box.pack_start(m_Entry, Gtk::PACK_EXPAND_WIDGET);
  m_Box.show_all();
}

AppExample::~AppExample()
{
}

void AppExample::init()
{
  type_vecStrings vecAuthors;
  vecAuthors.push_back("Murray Cumming <murrayc@usa.net>");
  set_about_information("0.1", vecAuthors, _("(C) 2000 Murray Cumming"), _("A Bakery example"));

  //Call base method:
  Bakery::App_Gtk::init();

  add(m_Box);
}

Bakery::App* AppExample::new_instance()
{
  AppExample* pApp = new AppExample();
  return pApp;
}

