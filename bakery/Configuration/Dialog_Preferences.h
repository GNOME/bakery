/*
 * Copyright 2002 Murray Cumming
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef BAKERY_CONFIGURATION_DIALOG_PREFERENCES_H
#define BAKERY_CONFIGURATION_DIALOG_PREFERENCES_H

#include <libglademm.h>
#include <bakery/Configuration/Client.h>
#include <gtkmm/dialog.h>

namespace Bakery
{

/** Preferences Dialog
 * In the Glade file:
 * - The vbox should contain a widget called "vbox".
 *
 * Use connect_widget(conf_key, widget_name) in the constructor.
 *
 * This is the base class for preferences dialogs and
 * supports only Gtk widgets. The Dialog_Preferences_GnomeUI
 * class in the bakery_gnomeui module supports Gnome::UI widgets in
 * addition to Gtk widgets.
 */
class Dialog_Preferences : public Gtk::Dialog
{
public:
  /// if instant is true then this will be an instant-apply preference dialog.
  Dialog_Preferences(Gtk::Window& parent, const Glib::ustring& configuration_directory, const Glib::ustring& glade_filename, const Glib::ustring& widget_name = "vbox", bool instant = false);
  virtual ~Dialog_Preferences();

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  virtual void load();
  virtual void save();
#else
  virtual void load(std::auto_ptr<Glib::Error>& error);
  virtual void save(std::auto_ptr<Glib::Error>& error);
#endif

protected:

   //Signal handlers:
  virtual void on_button_help();

  virtual void connect_widget(const Glib::ustring& key, const Glib::ustring& glade_widget_name);

  virtual void on_show(); //override.
  virtual void on_hide(); //override.
  virtual void on_response(int response_id); // override.

  Gtk::Button m_Button_Close, m_Button_Cancel, m_Button_Help;

  bool m_instant;
  Glib::RefPtr<Gnome::Glade::Xml> m_refGlade;
  Bakery::Conf::Client* m_pConfClient;
};


} //namespace Bakery

#endif //BAKERY_CONFIGURATION_DIALOG_PREFERENCES_H
