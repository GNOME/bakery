/*
 * Copyright 2002 Murray Cumming
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"
#include "bakery/Configuration/Dialog_Preferences.h"
#include <gtkmm/stock.h>
#include <glibmm/i18n-lib.h>

namespace Bakery
{


Dialog_Preferences::Dialog_Preferences(Gtk::Window& parent, const Glib::ustring& configuration_directory, const Glib::ustring& glade_filename, const Glib::ustring& widget_name, bool instant)
  : Gtk::Dialog(_("Preferences"), parent),
    m_Button_Close(Gtk::Stock::CLOSE),
    m_Button_Help(Gtk::Stock::HELP),
    m_instant(instant)
{
  set_border_width(12); //HIG-recommended.

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  m_refGlade = Gnome::Glade::Xml::create(glade_filename, widget_name);
#else
  std::auto_ptr<Gnome::Glade::XmlError> error;
  m_refGlade = Gnome::Glade::Xml::create(glade_filename, widget_name, "", error);
  // TODO: Error handling?
#endif

  // Add the glade-designed UI to the dialog:
  Gtk::Widget* pWidget = 0;
  m_refGlade->get_widget(widget_name, pWidget);
  if(pWidget)
    get_vbox()->pack_start(*pWidget);

  // Add the buttons:
  add_button(Gtk::Stock::CLOSE, Gtk::RESPONSE_CLOSE);
  add_button(Gtk::Stock::HELP, Gtk::RESPONSE_HELP);

  // Create a default Client instance. This may be replaced by
  // derived classes by assigning a new value to m_pConfClient,
  // in which case the instance create here should be deleted.
  m_pConfClient = new Bakery::Conf::Client(configuration_directory);

#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_show().connect(sigc::mem_fun(*this, &Dialog_Preferences::on_show));
  signal_hide().connect(sigc::mem_fun(*this, &Dialog_Preferences::on_hide));
  signal_response().connect(sigc::mem_fun(*this, &Dialog_Preferences::on_response));
#endif
}

Dialog_Preferences::~Dialog_Preferences()
{
  if(m_pConfClient)
  {
    delete m_pConfClient;
    m_pConfClient = 0;
  }
}

void Dialog_Preferences::connect_widget(const Glib::ustring& key, const Glib::ustring& glade_widget_name)
{
  Gtk::Widget* pWidget = 0;
  m_refGlade->get_widget(glade_widget_name, pWidget);
  if (pWidget && m_pConfClient)
  {
    if(m_instant)
      m_pConfClient->add_instant(key, *pWidget);
    else
      m_pConfClient->add(key, *pWidget);
  }
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Dialog_Preferences::load()
#else
void Dialog_Preferences::load(std::auto_ptr<Glib::Error>& error)
#endif
{
  if (m_pConfClient)
#ifdef GLIBMM_EXCEPTIONS_ENABLED
    m_pConfClient->load();
#else
    m_pConfClient->load(error);
#endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Dialog_Preferences::save()
#else
void Dialog_Preferences::save(std::auto_ptr<Glib::Error>& error)
#endif
{
  if (m_pConfClient)
#ifdef GLIBMM_EXCEPTIONS_ENABLED
    m_pConfClient->save();
#else
    m_pConfClient->save(error);
#endif
}

void Dialog_Preferences::on_show()
{
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  load();
#else
  std::auto_ptr<Glib::Error> error;
  load(error);
#endif

#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  Gtk::Dialog::on_show();
#endif
}

void Dialog_Preferences::on_hide()
{
  if (!m_instant)
  {
#ifdef GLIBMM_EXCEPTIONS_ENABLED
    save();
#else
    std::auto_ptr<Glib::Error> error;
    save(error);
#endif
  }
    
#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  Gtk::Dialog::on_hide();
#endif
}

void Dialog_Preferences::on_response(int response_id)
{
  switch(response_id)
  {
    case(Gtk::RESPONSE_HELP):
    {
      on_button_help();
      break;
    }
    case(Gtk::RESPONSE_CLOSE):
    default:
    {
      hide();
#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
      Gtk::Dialog::on_response(response_id);
#endif
      break;
    }
  }
}

void Dialog_Preferences::on_button_help()
{
  //TODO.
}

} //namespace Bakery

